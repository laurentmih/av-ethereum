//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Top level variables that need to be customised to each install on ether.camp
var sandboxID = 'de0ddfb57d'; //Grab it from when you run hello.sol, at the top of the green box that appears
var userName = '62275e9a'; // Your ether.camp username, usually what comes right after the https:// in your URL bar
var sandboxURL = 'https://' + userName + '.by.ether.camp:8555/sandbox/' + sandboxID; // These are just defaults in the way Ether camp is setup

// Including the correct instances/packages
var express = require('express'); // The webserver, available at https://[username].by.ether.camp:8080
var app = express(); // Instantiating our entire app (basically the script below)

var Web3 = require('web3'); // JS interface to Ethereum nodes
var web3 = new Web3(new Web3.providers.HttpProvider(sandboxURL)); // Call new instance of interface

var bodyParser = require('body-parser'); // For parsing POST web requests
var fs = require("fs"); // Filesystem access, I/O stuff for reading sourcecode

// Including some relevant middleware
app.use(express.static('public')); // For static files. 'public' is the name of the directory containing these static files. Should be located in same directory as index.js
app.use(bodyParser.json()); // Support JSON encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // Support encoded bodies

web3.eth.defaultAccount = web3.eth.accounts[0]; // Setting a default account. Because we need to run these transacions off _someone's_ accout
//In reality, this would have to be an unlocked account, though I don't fully understand the mechanism behind this (yet?).


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Starting the Express Node.js webserver
app.listen(8080, function() {
    console.log('Express server is up at https://' + userName + '.by.ether.camp:8080/ !');
});

// Setting up the homepage
app.get('/', function(req, res) {
    res.sendFile('/index.html', {root: __dirname });
});

// Reading in the source code of the contract that our 'create election' page will be creating
fs.readFile("./contracts/AvVote.sol", 'utf8', function(error, data) {
    if(error) {
        console.log("Could not find your contract file! Are you sure you've named it correctly?");
    } else {
        var sourcecode = data;
        // We convert the source to bytecode which we will send each time to the chain when a contract gets created.
        // We also obtain the ABI, which we need so we can tell web3.js how to interact with the contract we created.
        var compiled = web3.eth.compile.solidity(sourcecode); // Which we get a few lines above by reading in the contract.sol
        
        /* 
        Look, I know I will probably end in programming hell for using globals, but bite me. Its a HACKathon, not a 'best coding practices programming event'
        I've included an alternative implementation of this below, but I'm not sure it works, and I can't be bothered to test this.
        If anyone is really insane enough to actually want to try this [[nervous laughter increases]], you'll probably want to look at the alternative implementation -Laurent
        */
        global.bytecode = compiled.AvVote.code; // TODO: We need to change this to the actual contract name as specified in the .sol file!
        global.abi = compiled.AvVote.info.abiDefinition;
    }
});

/*
// Alternative implementation to the readFile() function above. I can't be bothered to check if this works, but I think it's a step in the right direction to avoid globals
// which is apparently bad Node.js etiquette.
var sourcecode = fs.readFile("./contracts/hello.sol", 'utf8');
var compiled = web3.eth.compile.solidity(sourcecode);
global.bytecode = compiled.hello.code;
global.abi = compiled.hello.info.abiDefinition;
// P.S. Fuck synchronous JS I just don't get how the hell it's supposed to work
*/

// On the 'create election' page, if you submit the form, this is where the request with your data goes (over POST)
app.post('/createelection', function(req, res) {
    var numCand = req.body.numCand; // Number of candidates 
    var timeLimit = req.body.timeLimit; // Time limit of the contract (in seconds)
    var fee = req.body.fee;
    
    // if (req.body.freeForAll) {
    //     var freeForAll = req.body.freeForAll; // Radio button 'free for all' was selected
    // } else if (req.body.limitedEl) { // Other radio button was selected
    //     var limitedEl = req.body.limitedEl; 
    //     var keys = req.body.keys; // The amount of keys that need to be generated (amount of addresses that are allowed to vote)
    // }
    
    // Time to start the contract party, we're going to build a contract along with the specifications from the POST form
    //web3.eth.contract(abi).new(numCand, timeLimit, {from: web3.eth.defaultAccount, data: bytecode, gas: 1000000}, function (error, contract) { // Pass constructor arguments
    web3.eth.contract(global.abi).new(timeLimit, numCand, fee, {from: web3.eth.defaultAccount, data: global.bytecode, gas: 1000000}, function (error, contract) { // For testing with hello.sol
        if (!error && contract.address) { // If contract creation was succesful
            console.log("Contract instantiated at address: " + contract.address);
            res.send({message:"success", address:contract.address}); // For AJAX response
        }
        else if (error) { // If something went wrong
            console.log("There's been an error, shown below:");
            console.log(error);
            res.send(error);
        }
    });
});

// On the 'cast vote' page, when you click 'Fetch!', this is where your request goes
app.post('/fetchcontract', function(req, res) {
  var contAddr = req.body.contAddr;
  //console.log('The bytecode running after fetch: ' + web3.eth.getCode(contAddr))
  if (web3.eth.getCode(contAddr).length > 2) { // This call basically requests the code running at the given address. If there's no code (i.e. no contract) it'll return '0x'.
      res.send({message: 'success'});
  } else {
      res.send({message: 'error', log : "No contract running at that address!"});
  }
});

// Function called when a vote is cast
app.post('/castvote', function(req, res) {
    var contAddr = req.body.contAddr;
    var sendAddr = req.body.sendAddr;
    var fee = req.body.fee;
    var prefs = req.body.prefs.replace(/\s+/g, '').split(',').map(Number);
    
    var filtOpt = {address : contAddr}; // Defining the filter options for error handling
    var filter = web3.eth.filter(filtOpt); // Instantiating the filter with the specified options (just the address of the smart contract)
    
    var contractObject = web3.eth.contract(abi);
    var contractInstance = contractObject.at(contAddr);
    
    // Call the vote() method in the smart contract AvVote.sol
    contractInstance.vote(prefs, {value: fee, gas: 3000000},function(error, response) {
        //console.log('Response: ' + response);
        var receipt = web3.eth.getTransactionReceipt(response);
        //console.log('Receipt: ' + JSON.stringify(receipt));
        
        if (receipt.logs.length == 0) { // If there's no logs, that means that the transaction went fine (vote() will not throw an event)
            res.send({message: 'success'});
        } else { // If something went wrong, vote() will throw an event, and we have to go check out the logs
            filter.watch(function(error, data) { // If the filter finds a transaction with the smart contract's address, it will trigger this function
                if (data.topics[2]) { // The smart contract will issue the error codes in the last byte of the 3rd topic of the logs
                    var statusInt = parseInt(data.topics[2].slice(-1)); // Check the last byte of the topic
                    if (statusInt == 0) {
                        error = 'Not enough value was sent!';
                    } else if (statusInt == 1) {
                        error = 'Sorry, you are too late, the election has already ended.'
                    } else if (statusInt == 2) {
                        error = 'Invalid input!';
                    }
                    res.send({message: 'error', log : error});
                }
            });
        };
    });
});

// Function called when the winner of a contract is requested
app.post('/fetchwinner', function(req, res) {
    var contAddr = req.body.contAddr;
    var contractObject = web3.eth.contract(abi);
    var contractInstance = contractObject.at(contAddr);
    //console.log('Received a fetchwinner request for address: ' + contAddr)
    
    contractInstance.calcResult(function(error, response) { // call the calcResult() constant method in the smart contract
        if (!error) {
            res.send({message : 'success', winner : response}); // Response will be the int of the winning candidate
        }
        else {
            res.send({message : 'error', log : error})
        }
    });
});