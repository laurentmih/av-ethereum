contract AvVote {

    // Structure of the stored ballot
    struct Ballot {
        address voter;
        uint numberOfRanks;
        // mapping a rank to a candidate
        mapping(uint => uint) ranks;
    }

    // Enumerater for easily defining the cause of the rejection.
    // Will be stroed as simple uint (error code).
    enum CauseForRejection {
        FeeNotPaid,
        VoteIsOver,
        BallotNotValid
    }
    // Event which is used by the front end to detect wether a ballot has been accepted or not.
    event BallotRejected(
        address indexed sender,
        CauseForRejection indexed cause
    );

    // Moment after which no further ballots are accepted.
    uint endOfVote;
    // Fee to paid to insert a ballot
    uint minimalFee;
    // Number of candidates numbered from 0 to numberOfCandidates-1
    uint numberOfCandidates;
    // Mapping to assign some meaning to the candidates
    mapping(uint => string) candidateNames;
    // Who created this contract
    address owner;
    
    // The ballots of this vote
    uint numberOfBallots;
    mapping(uint => Ballot) ballots;

    // Constructor for this smart contract
    function AvVote(uint _timeToVote, uint _numberOfCandidates, uint _minimalFee) {
        owner = msg.sender;
        endOfVote = now + _timeToVote;
        minimalFee = _minimalFee;
        numberOfCandidates = _numberOfCandidates;
        minimalFee = _minimalFee;
    }
    
    function getMinimalFee() constant returns(uint f) {
        return minimalFee;
    }
    
    function getNameOfCandidate(uint _id) constant returns(string name) {
        return candidateNames[_id];
    }
    
    function setNameOfCandidate(uint _id, string _name) {
        if(msg.sender != owner) {
            throw;
        }
        candidateNames[_id] = _name;
    }
    
    function voteIsOpen() constant returns(bool open) {
        return now < endOfVote;
    }
    
    function remainingTime() constant returns(uint time) {
        return endOfVote - now;
    }

    // Function that makes sure that the sender receives his funds back.
    // Also it creates an event informing it about the cause of rejection.
    function rejectBallot(CauseForRejection cause) private {
        BallotRejected(msg.sender, cause);
        bool val = msg.sender.send(msg.value);
    }

    modifier payMinimalFee() {
        if (msg.value < minimalFee) {
            rejectBallot(CauseForRejection.FeeNotPaid);
        }
        else {
            _
        }
    }

    modifier voteIsNotOver() {
        if (now > endOfVote) {
            rejectBallot(CauseForRejection.VoteIsOver);
        }
        else {
            _
        }
    }

    modifier correctPreferences(uint[] _preferences) {
        bool[] memory awardedCandidates = new bool[](numberOfCandidates);
        for (uint i = 0; i < _preferences.length; i++) {
            if (_preferences[i] < numberOfCandidates) {
                if (!awardedCandidates[i]) {
                    awardedCandidates[i] = true;
                }
                else {
                    rejectBallot(CauseForRejection.BallotNotValid);
                }
            }
            else {
                rejectBallot(CauseForRejection.BallotNotValid);
            }
        }
        _
    }

    function vote(uint[] _preferences) payMinimalFee voteIsNotOver correctPreferences(_preferences) {
        bool val = owner.send(msg.value);
        ballots[numberOfBallots].voter = msg.sender;
        ballots[numberOfBallots].numberOfRanks = _preferences.length;
        for (uint i = 0; i < _preferences.length; i++) {
            ballots[numberOfBallots].ranks[i] = _preferences[i];
        }
        numberOfBallots++;
    }

    function calcResult() constant returns(uint winner) {
        bool[] memory eliminatedCandidates = new bool[](numberOfCandidates);
        uint numberOfCountedBallots;
        bool foundWinner = false;

        while (!foundWinner) {
            uint[] memory result = new uint[](numberOfCandidates);
            uint numberOfValidVotes = 0;

            for (uint b = 0; b < numberOfBallots; b++) {
                for (uint r = 0; r < ballots[b].numberOfRanks; r++) {
                    uint k = ballots[b].ranks[r];
                    if (!eliminatedCandidates[k]) {
                        result[k] = result[k] + 1;
                        numberOfCountedBallots++;
                        break;
                    }
                }
            }



            uint weakestCandidate;
            uint threshold = numberOfCountedBallots/2-1;
            for (uint c = 0; c < numberOfCandidates; c++) {
                if (result[c] > threshold) {
                    foundWinner = true;
                    return c;
                    break;
                }
                if (result[c] < result[weakestCandidate]) {
                    weakestCandidate = c;
                }
            }
            eliminatedCandidates[weakestCandidate] = true;
        }

    }
}