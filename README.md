# AV - Alternative voting system on the Ethereum Blockchain #

## Alternative Voting System ##
The alternative voting system is based on the promise of preferences, instead of a first past the post type of deal. For more information, check out this YouTube video that explains it pretty well: https://www.youtube.com/watch?v=3Y3jE3B8HsE

## Blockchain? ##
The great thing about the blockchain, this project uses the Ethereum one, is that it's public. Everyone can check that the voting is being counted correctly, thanks to the smart contract. However, this also means that the votes themselves are public as well (at least, in our implementation they are for now, see below for our proposals on how to remedy that).

## The project ##
To get started on the project:
1. Head over to https://live.ether.camp/ and sign up. This will land you an ubuntu VM with them. 
2. Create a new folder called 'AV'
3. Launch a terminal, and pull this git repo
```
git pull https://gitlab.com/2fat2kidnap/av-ethereum.git
```
4. Open up AvVote, which you can find in `/workspace/AV/contracts`
5. At the top of your page, with the AvVote.sol open, click 'Start sandbox'
6. Grab the Sandbox ID, at the top right of the page.
7. Open up `/workspace/AV/index.js` and change lines 4 and 5, replace them with the Sandbox ID and your ether.camp username.
```
var sandboxID = 'd8f359fc00';
var userName = '62275e9a';
```
8. Save `index.js`
9. In terminal, type:
```
npm install
node .
```
10. This should spit out the URL, so you can go check it out.

## Privacy concerns ##
We figured that the votes could be made less public using asymmetric crypto. The 'creator' of the election could generate a public/private keypair. He hands out the public key, tells everyone to encrypt their vote, along with some noise, and send it to the smart contract, which stores it.
Once the vote is finished, the private key is released, everyone can decrypt all replies, and the votes can be counted by everyone who has the private key (which is now made public).
We have not put 100 hours of thought into this though, so it might be entirely mistaken, but it was something we came up with.